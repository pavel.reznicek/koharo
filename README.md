# koharo

A font featuring the Mishay alphabet, Koharo Mishayego.

Used in the fantasy literary work of Pavel Řezníček A. K. A. Cigydd.

See https://bitbucket.org/cigydd/misajove.

The metafont source (koharo.mf) should be placed in the `/fonts/source/public/koharo` folder in your texmf tree.